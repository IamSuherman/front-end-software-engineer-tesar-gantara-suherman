import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Tambah from '../views/Tambah.vue'
import Lihat from '../views/Lihat.vue'
import Edit from '../views/Edit.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/tambah',
    name: 'Tambah',
    component: Tambah
  },
  {
    path: '/lihat/:id',
    name: 'Lihat',
    component: Lihat
  },
  {
    path: '/:id/',
    name: 'Edit',
    component: Edit
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
